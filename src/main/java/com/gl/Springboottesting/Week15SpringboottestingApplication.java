package com.gl.Springboottesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week15SpringboottestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week15SpringboottestingApplication.class, args);
	}

}
